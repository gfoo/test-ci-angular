import { Component, OnInit, isDevMode } from '@angular/core';
const { version: appVersionLoaded } = require('../../package.json');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  appVersion: string = appVersionLoaded;
  devMode: string = isDevMode() ? ' dev mode' : '';
}
