#!/bin/bash

. release.config

git checkout -b release-$APP_VERSION develop
yarn version --no-git-tag-version --new-version $APP_VERSION
git commit -a -m "$APP_VERSION Release"
docker build -t $IMAGE .
docker run -p 80:80 $IMAGE
