#!/bin/bash

. release.config

git checkout master
git merge --no-ff release-$APP_VERSION
git tag -a $APP_VERSION -m "$APP_VERSION Release"
git push
git push --tags

docker build -t $IMAGE:$APP_VERSION .
docker push $IMAGE:$APP_VERSION

git checkout develop
git merge --no-ff release-$APP_VERSION
yarn version --no-git-tag-version --new-version $NEXT_DEV_APP_VERSION
git commit -a -m "$NEXT_DEV_APP_VERSION in dev"
git push
git branch -d release-$APP_VERSION
